#!/usr/bin/python3
import sys

# j = columns
# i = rows
# c[i-1][j] = go up on the table
# c[i][j-1] = go left on the table
# c[i-1][j-1] = go diag-upper left on the table
def edit_distance(X,Y):
    m = len(X)
    n = len(Y)
    c = [[0] * (n+1) for i in range(m+1)]

    for i in range(m+1):
        c[i][0] = i

    c[0] = [j for j in range(n+1)]        
    
    b = [['']*(n+1) for i in range(m+1)]

    
    for j in range(1, n+1):
        b[0][j] = 'i'
    for i in range(1, m+1):
        b[i][0] = 'd'

    for i in range(1, m+1):
        for j in range(1, n+1):
            if X[i-1] == Y[j-1]:
                c[i][j] = c[i-1][j-1]
                b[i][j] = 'm'
            elif c[i-1][j] <= c[i-1][j-1] and c[i-1][j] <= c[i][j-1]:
                c[i][j] = c[i-1][j] + 1
                b[i][j] = 'd'
            elif c[i-1][j-1] <= c[i][j-1]:
                c[i][j] = c[i-1][j-1] + 1
                b[i][j] = 's'
            else:
                c[i][j] = c[i][j-1] + 1
                b[i][j] = 'i'
    
    return {'c': c, 'b':b}

def printops_ed(res, X, Y):

    edits = ""
    i = 0
    j = 0
    while i < len(X) or j < len(Y):
        if i < len(X) and j < len(Y) and res['c'][i+1][j+1] == res['c'][i][j]:
            edits += res['b'][i+1][j+1]
            i += 1
            j += 1
        elif i < len(X) and j < len(Y) and res['c'][i+1][j+1] == res['c'][i][j] + 1:
            edits += res['b'][i+1][j+1]
            i += 1
            j += 1
        elif j < len(Y) and res['c'][i][j+1] == res['c'][i][j] + 1:
            edits += res['b'][i][j+1]
            j += 1
        else:
            edits += res['b'][i+1][j]
            i += 1
            
        
    for i in range(max(len(X)+1, len(Y)+1)):
        if i == 0:
            print(Y[:i] + "_" + X[i:])
        else:
            print(Y[:i] + "_" + X[i:], "({0})".format(edits[i-1]))

    print()
    
def printtable_ed(res, X, Y):
    #number table
    for j in range(len(Y)):
        if j == 0:
            print("{0:4s}".format(""), end='')
        print("{0:2s}".format(Y[j]), end='')
    print()    
    for i in range(len(X)+1):
        if i == 0: print(" ", end='')
        for j in range(len(Y)+1):
            if j == 0 and 0 < i < len(X)+1:
                print("{0}".format(X[i-1]), end='')
            print("{0:2d}".format(res['c'][i][j]), end='')
        print()
    print()

    # edit table
    for j in range(len(Y)):
        if j == 0:
            print("{0:4s}".format(""), end='')
        print("{0:2s}".format(Y[j]), end='')
    print()
    for i in range(len(X)+1):
        if i == 0: print("{0:2s}".format(""), end='')
        for j in range(len(Y)+1):
            if j == 0 and 0 < i < len(X) + 1:
                print("{0} ".format(X[i-1]), end='')
            print("{0:2s}".format(res['b'][i][j]), end='')
        print()
    print()


            
if __name__ == '__main__':

    X = "wonderful"
    Y = "worldwideweb"


    res = nw_align(X, Y)


    print_nw_align_iter(res['c'], X, Y, len(X), len(Y))
    printtable_nw(res, X, Y, len(X), len(Y))

                
if __name__ == '__main__':

    X = "super"
    Y = "paper"

    res = edit_distance(X,Y)
    print(res['c'][len(X)][len(Y)])     
