#!/usr/bin/python3

import sys, random, argparse, time, datetime
import p1_algs

verbose = '''verbose levels: 0 - numeric answer only, 1 - include alignment, 2 - include DP table / lists used in computation'''

# set up the command-line arguments
parser = argparse.ArgumentParser(description = 'p1 - string algorithms toolkit')
group_x = parser.add_mutually_exclusive_group(required=True)
group_x.add_argument('--x', default = None, type=str, help='first string')
group_x.add_argument('--x_file', type=argparse.FileType('r'), help='file to read to use as first string')
group_y = parser.add_mutually_exclusive_group(required=True)
group_y.add_argument('--y_file', type=argparse.FileType('r'), help='second string')
group_y.add_argument('--y', default = None, type=str, help='file to read as second string')
parser.add_argument('--alg', default = '', choices= p1_algs.algorithms.keys(),
                    type=str, help=p1_algs.help, required=True)
parser.add_argument('--verbose', default=0, choices=[0,1,2],
                    type=int, help=verbose, required=False)

# parse the command-line arguments
args = parser.parse_args()

# strings will be X and Y
if args.x: X = args.x
else: X = args.x_file.read()

if args.y: Y = args.y
else: Y = args.y_file.read()

# and call the appropriate algorithm based on the choice in algorithm
p1_algs.algorithms[args.alg](X, Y, args.verbose)
