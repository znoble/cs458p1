import sys
sys.path.append('/u1/h0/jkinne/public_html/cs458-f2020/code//GENERIC')
from all_subsets import *

def lcsnaive(X, Y):
        
    Lx = first_subset()

    longest = ''
    longest_Lx = []
    longest_Ly = []

    while len(Lx) < len(X):
        Lx = next_subset(Lx, len(X))

        Ly = first_subset()
        while len(Ly) < len(Y):
            Ly = next_subset(Ly, len(Y))

            # Lx and Ly
            s_x = string_subset(Lx, X)
            s_y = string_subset(Ly, Y)
            if s_x == s_y and len(s_x) > len(longest):
                longest = s_x
                longest_Lx = Lx
                longest_Ly = Ly

    return {'longest': longest, 'longest_Lx': longest_Lx, 'longest_Ly': longest_Ly}


def printlcsnaive(X, Y, res):

    Lx = res['longest_Lx']
    Ly = res['longest_Ly']
    
    x = ""
    y = ""
    
    i = len(X)-1
    j = len(Y)-1
    c = len(Lx)-1
    while i >= 0 or j >= 0:
        if (i == Lx[c] and j == Ly[c]) or (c < 0 and j == i):
            x = X[i] + x
            y = Y[j] + y
            i -= 1
            j -= 1
            c -= 1
        elif j == Ly[c] or (c < 0 and i > j):
            x = X[i] + x
            y = " " + y
            i -= 1
        else:
            x = " " + x
            y = Y[j] + y
            j -= 1            
    
    print(x)
    for i in range(len(x)):
        if x[i] == y[i]:
            print("|", end='')
        else:
            print(" ", end='')
    print()
    print(y)


if __name__ == "__main__":

    X = "GGAAATCCGC"
    Y = "CATGAACC"
    
    res = lcsnaive(X, Y)
    print(len(res['longest']))
    
    printlcsnaive(X, Y, res)                
            
        
            
            
        
        
