import sys
sys.path.append('/u1/h0/jkinne/public_html/cs458-f2020/code//GENERIC')
from all_subsets import *
from lcsnaive import *
from lcsdp import *
from ed import *
from nw import *
from sw import *
from matchnaive import *
from rk import *
# ./p1.py --x TTCGAAG --y TCAG --alg lcs_naive
# ./p1.py --x GGAAATCCGC --y CATGAACC --alg lcs_naive
def lcs_naive(X, Y, verbose):

    res = lcsnaive(X, Y)
    print(len(res['longest']))


    if verbose > 0: 
        printlcsnaive(X, Y, res)
    
# ./p1.py --x_file /u1/junk/kinne/genomes/gene_transcripts/GRCh38_latest_rna.fna.100k --y "GACCCCAAAATCAGCGAAAT" --alg lcs_dp    
def lcs_dp(X, Y, verbose):
    res = lcs_length(X, Y)
    print(res['c'][len(X)][len(Y)])

    if verbose > 0:
        print_lcsdp(res['c'], X, Y)
        print()
    if verbose > 1:
        printtable_lcsdp(res['c'], X, Y)
        print()
        printtable_lcsdp(res['b'], X, Y)


# ./p1.py --alg edit_dp --x "hello" --y "help"
def edit_dp(X, Y, verbose):
    res = edit_distance(X, Y)
    print(res['c'][len(X)][len(Y)])

    if verbose > 0:
        printops_ed(res, X, Y)
    if verbose > 1:
        printtable_ed(res, X, Y)


# ./p1.py --alg align_NW --x GCATGCU --y GATTACA
def align_NW(X, Y, verbose):
    res = nw_align(X, Y)
    print(res['c'][len(X)][len(Y)])
    
    if verbose > 0:
        print_nw_align_iter(res['c'], X, Y, len(X), len(Y))
    if verbose > 1:
        printtable_nw(res, X, Y, len(X), len(Y))
        
    
def align_SW(X, Y, verbose):

    res = sw_align(X, Y)
    print(res['MAX'])

    if verbose > 0:
        print_sw_align(res['c'], X, Y, res['MAX_loc'])
    if verbose > 1:
        print_sw_table(res['c'], X, Y)

        
def match_naive(X, Y, verbose):
    res = matchnaive(X, Y)
    print_matchnaive(res)

    if verbose > 0:
        print_matchnaive_align(res, X, Y)
    
def match_RK(X, Y, verbose):
    loc = RabinKarp(X, Y)
    print_rk(loc)
    
    if verbose > 0:
          print_rk_align(loc, X, Y)
    
          
algorithms = {'lcs_naive': lcs_naive,
              'lcs_dp': lcs_dp,
              'edit_dp': edit_dp,
              'align_NW': align_NW,
              'align_SW': align_SW,
              'match_naive': match_naive,
              'match_RK': match_RK}

help = '''lcs_naive: computes LCS using brute force (from class).
lcs_dp: uses dynamic programming (algorithm from the text, code from class).  
edit_dp: computes the edit distance using dynamic programming (as described in wikipedia, with unit-cost operations insert, delete, substitute, transpose).  
align_NW: aligns strings using the Needleman–Wunsch algorithm as described on wikipedia (indel or mismatch -1, match +1).  
align_SW: aligns strings using the Smith–Waterman algorithms as described on wikipedia.  
match_naive: string matching using naive algorithm from text/classe (code from class).
match_RK: string matching using RK from the text.'''
