#implementation of the Rabin-Karp algorithm for string matching

import sys

#  T = Target String
#  P = Pattern String
#  d = numerical base (radix)
#  q = prime number for modulus
def RabinKarp(T, P, d=4, q=997):
    n = len(T)
    m = len(P)
    h = d**(m-1) % q
    p = 0
    t = 0
    for i in range(m):
        p = (d*p + ord(P[i])) % q

        t = (d*t + ord(T[i])) % q
    for i in range(n-m):
        if p == t and P == T[i : i + m]:
            return i
        t = (d*(t - ord(T[i])*h) + ord(T[i+m])) % q
    return None

def print_rk(i):
    if i != None:
        print("Pattern found at {0}.".format(i))
    else:
        print("Not found.")

def print_rk_align(i, X, Y):
    if i:
        print(X)
        print(" "*i + Y)

## Testing ##
if __name__ == "__main__":
    target = "CCCAGATCCAAGAAGAAGGAACAGTGGTGGAATTGACTGGAAGGCAGTCCAGTGAAATCACAAG"
    pattern = "CAGTGAA"
    radix = 4
    q = 997

    loc = RabinKarp(target, pattern, radix, q)
    if loc != None:
        print("Pattern occurs at index {0}".format(loc))
        print("P = {0}".format(pattern))
        print("T[{0}:{1}] = {2}".format(loc, loc + len(pattern), target[loc:loc+len(pattern)]))
    else:
        print("Not found.")

        

    
