#!/urs/bin/python3
#examples
# x: "hello"
# y: "this is good"
#
# x: "GGAAATCCGC"
# y: "CATGAACC"

import sys

def lcs_length(X, Y):
    m = len(X)
    n = len(Y)
    c = [[0]*(n+1) for i in range(m+1)]
    b = [['']*(n+1) for i in range(m+1)]

    for i in range(1, m+1):
        for j in range(1, n+1):
            if X[i-1] == Y[j-1]:
                c[i][j] = c[i-1][j-1] + 1
                b[i][j] = '\\'
            elif c[i-1][j] >= c[i][j-1]:
                c[i][j] = c[i-1][j]
                b[i][j] = '|'
            else:
                c[i][j] = c[i][j-1]
                b[i][j] = '-'
    return {'c':c, 'b':b}


def print_lcsdp(table, X, Y):
    localX = ""
    localY = ""
    i = len(X)
    j = len(Y)

    while i > 0 or j > 0:   

        if i > 0 and j > 0 and X[i-1] == Y[j-1]:
            localX = X[i-1] + localX
            localY = Y[j-1] + localY
            i -= 1
            j -= 1
        elif i > 0 and j > 0 and table[i-1][j-1] == table[i-1][j]:
            localX = X[i-1] + localX
            localY = Y[j-1] + localY
            i -= 1
            j -= 1
        elif i > 0 and table[i-1][j] >= table[i][j-1]:
            localX = X[i-1] + localX
            localY = " " + localY
            i -= 1
        else:
            localX = " " + localX
            localY = Y[j-1] + localY
            j -= 1
                
    print(localX)
    for i in range(len(localX)):
        if localX[i]==localY[i]:
            print("|", end='')
        else:
            print(" ", end='')
    print()
    print(localY)

def printtable_lcsdp(table, X, Y):


    for j in range(len(Y)):
        if j == 0:
            print("    ", end='')
        print("{0:2s}".format(Y[j]), end='')
    print()
    
    for i in range(len(X)+1):
        for j in range(len(Y)+1):
            if j == 0 and i != 0:
                print("{0:2s}".format(X[i - 1]), end='')
            elif j == 0 and i == 0:
                print("  ", end='')
            print("{0:2s}".format(str(table[i][j])), end='')
        print()
    print()
    
    


##Testing##
if __name__ == "__main__":

# X=' h    ello'
# Y='this is good'
#     +       +
#    
    X = "GGAAATCCGC"
    Y = "CATGAACC"

    res = lcs_length(X, Y)
    print(res['c'][len(X)][len(Y)])
    print()

    #tables
    printtable_lcsdp(res['c'], X, Y)
    printtable_lcsdp(res['b'], X, Y)
    
    
    #matches
    print_lcsdp(res['c'], X, Y)

