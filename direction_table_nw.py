def print_sw_X(b, X, i, j):
    if i==0 or j==0:
        return
    if b[i][j] == '\\':
        print_sw_X(b, X, i-1, j-1)
        print(X[i-1], end='')
    elif b[i][j] == '|':
        print_sw_X(b, X, i-1, j)
        print(X[i-1], end='')
    elif b[i][j] == '-':
        print_sw_X(b, X, i, j-1)
        print('-', end='')

def print_sw_Y(b, Y, i, j):
    if i==0 or j==0:
        return
    if b[i][j] == '\\':
        print_sw_Y(b, Y, i-1, j-1)
        print(Y[j-1], end='')
    elif b[i][j] == '|':
        print_sw_Y(b, Y, i-1, j)
        print('-', end='')
    elif b[i][j] == '-':
        print_sw_Y(b, Y, i, j-1)
        print(Y[j-1], end='')

def print_sw_align(b, X, Y, i, j):

    print_sw_X(b, X, i, j)
    print()
    print_sw_Y(b, Y, i, j)
    print()
