import sys

def matchnaive(X, Y):    
    for p in range(0, len(X)):
        found = True
        for k in range(0, len(Y)):
            if Y[k] != X[p+k]:
                found = False
                break
        if found:
            return p
    return None

def print_matchnaive(p):
    if p:
        print("Found at position {0}".format(p))
    else:
        print("Not found.")

def print_matchnaive_align(p, X, Y):
    if p:
        print(X)
        print(" "*(p-1) + Y)
   
        
if __name__ == "__main__":

    X = "GGGACCAGATGGATTGTAGGGAGTAGGGTACAATACAGTCTGTTCTCCTCCAGCTCCTTCTTTCTGCAACATGGGGAAGA"
    Y =                                                                      "CAT"

    p = matchnaive(X, Y)
    print_matchnaive(p)
    print_matchnaive_align(p, X, Y)
    
