import sys
sys.path.append('../GENERIC')
from all_subsets import *

#    0123456
X = 'TTCGAAG'
Y = 'TCAG'

#    0123456
X = 'GGAAATCCGC'
Y = 'CATGAACC'

#
X = "hello there"
Y = "this good"

Lx = first_subset()

longest = ''
longest_Lx = []
longest_Ly = []

while len(Lx) < len(X):
    #print(string_subset(Lx, X))
    Lx = next_subset(Lx, len(X))

    Ly = first_subset()
    while len(Ly) < len(Y):
        Ly = next_subset(Ly, len(Y))

        # Lx and Ly
        s_x = string_subset(Lx, X)
        s_y = string_subset(Ly, Y)
        if s_x == s_y and len(s_x) > len(longest):
            longest = s_x
            longest_Lx = Lx
            longest_Ly = Ly

print(longest, longest_Lx, longest_Ly)