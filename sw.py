import sys

def sw_align(X, Y):
    m = len(X)
    n = len(Y)
    c = [[0] * (n+1) for i in range(m+1)]   

    MAX = -1
    MAX_loc = [0,0]
    for i in range(1, m+1):
        for j in range(1, n+1):
            if X[i-1] == Y[j-1]:
                c[i][j] = c[i-1][j-1] + 1
            elif c[i-1][j-1] >= c[i-1][j] and c[i-1][j-1] >= c[i][j-1]:
                c[i][j] = c[i-1][j-1] - 1
            elif c[i-1][j] >= c[i][j-1]:
                c[i][j] = c[i-1][j] - 1
            else:
                c[i][j] = c[i][j-1] - 1

            if c[i][j] < 0:
                c[i][j] = 0

            if c[i][j] > MAX:
                MAX = c[i][j]
                MAX_loc = [i,j]

    return {'c': c, 'MAX': MAX, 'MAX_loc':MAX_loc}


def traceback_sw(c, X, Y, MAX_loc):
    localX = ""
    localY = ""
    i = MAX_loc[0]
    j = MAX_loc[1]
    while c[i][j] > 0 and i > 0 and j > 0:
        if X[i-1] == Y[j-1]:
            localX = X[i-1] + localX
            localY = Y[j-1] + localY
            i -= 1
            j -= 1

        elif c[i-1][j-1] >= c[i-1][j] and c[i-1][j-1] >= c[i][j-1]:
            localX = X[i-1] + localX
            localY = Y[j-1] + localY
            i -= 1
            j -= 1

        elif c[i-1][j] >= c[i][j-1]:
            localX = X[i-1] + localX
            localY = "-" + localY
            i -= 1

        else:
            localX = "-" + localX
            localY = Y[j-1] + localY
            j -= 1

        
        #test this later
        #val = max(c[i-1][j-1], c[i-1][j], c[i][j-1])
        #loc = [k,l if c[k][l] == val for k,l in [c[i-1][j-1],c[i-1][j],c[i][j-1]]]
    return localX, localY
        
#print alignment
def print_sw_align(c, X, Y, MAX_loc):
    
    x, y =traceback_sw(c, X, Y, MAX_loc)
    
    print(x)
    
    for i in range(len(x)):
        if x[i] == y[i]: print("|", end='')
        else: print(" ", end='')
    print()
    
    print(y)

def print_sw_table(c, X, Y):

    print(" "*5, end=' ')
    for j in range(len(Y)):
        print("{0:2s}".format(Y[j]), end=' ')
    print()
    for i in range(len(X)+1):
        for j in range(len(Y)+1):
            if j == 0 and i == 0:
                print(" ", end=' ')
            if j == 0 and 0 < i < len(X)+1:
                print("{0}".format(X[i-1]), end=' ')
            print("{0:2d}".format(c[i][j]), end=' ')
        print()
    print()

                   
if __name__ == '__main__':

    Y = "TGTTACGG"
    X = "GGTTGACTA"

    
#number solution
    res = sw_align(X, Y)
    print(res['MAX'])

#print alignment
    print_sw_align(res['c'], X, Y, res['MAX_loc'])

    
#table
    print_sw_tabl(res['c'], X, Y)

