import sys

def nw_align(X, Y):
    m = len(X)
    n = len(Y)
    c = [[0] * (n+1) for i in range(m+1)]

    for i in range(m+1):
        c[i][0] = -i

    c[0] = [-j for j in range(n+1)]        
    
    b = [['']*(n+1) for i in range(m+1)]

    for i in range(1, m+1):
        for j in range(1, n+1):
            if X[i-1] == Y[j-1]:
                c[i][j] = c[i-1][j-1] + 1
                b[i][j] = '\\'
            elif c[i-1][j-1] >= c[i-1][j] and c[i-1][j-1] >= c[i][j-1]:
                c[i][j] = c[i-1][j-1] - 1
                b[i][j] = '\\'
            elif c[i-1][j] >= c[i][j-1]:
                c[i][j] = c[i-1][j] - 1
                b[i][j] = '|'
            else:
                c[i][j] = c[i][j-1] - 1
                b[i][j] = '-'

    return {'c': c, 'b': b}

def print_nw_align_iter(c, X, Y, i, j):

    alignX = ""
    alignY = ""
    while i > 0 or j > 0:
        if i > 0 and j > 0 and X[i-1] == Y[j-1]: #c[i][j] == c[i-1][j-1] + 1:
            alignX = X[i-1] + alignX
            alignY = Y[j-1] + alignY
            i -= 1
            j -= 1
        elif i > 0 and j > 0 and c[i][j] == c[i-1][j-1] - 1:
            alignX = X[i-1] + alignX
            alignY = Y[j-1] + alignY
            i -= 1
            j -= 1
        elif i > 0 and c[i][j] == c[i-1][j] - 1:
            alignX = X[i-1] + alignX
            alignY = '-' + alignY
            i -= 1
        else:
            alignX = '-' + alignX
            alignY = Y[j-1] + alignY
            j -= 1

    print(alignX)
    for i in range(len(alignX)):
        if alignX[i] == alignY[i]:
            print('|', end='')
        else:
            print(' ', end='')
    print()
    print(alignY)

    

def printtable_nw(res, X, Y, i , j):

    for j in range(len(Y)):
        if j == 0:
            print("{0:8s}".format(""), end='')
        print("{0:4s}".format(Y[j]), end='')
    print()    
    for i in range(len(X)+1):
        if i == 0: print(" ", end='')
        for j in range(len(Y)+1):
            if j == 0 and 0 < i < len(X)+1:
                print("{0}".format(X[i-1]), end='')
            print("{0:4d}".format(res['c'][i][j]), end='')
        print()
    print()

            
if __name__ == '__main__':

    X = "wonderful"
    Y = "worldwideweb"


    res = nw_align(X, Y)


    print_nw_align_iter(res['c'], X, Y, len(X), len(Y))
    printtable_nw(res, X, Y, len(X), len(Y))
